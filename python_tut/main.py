from datetime import datetime
from typing import Any


def prompt(text: str, cast: type = str) -> Any:
    resp = input(text)
    try:
        c_resp = cast(resp)
        return c_resp
    except ValueError:
        print(f"Incorrect input! Please enter {str(cast)}")
        return prompt(text, cast)


class Bill:
    def __init__(self, customer_id: str) -> None:
        self.customer_id = customer_id
        self.date = datetime.now().date()
        self.options = ["(c) create item", "(s) save bill"]
        self.items = []
        self.total = 0
        self.tax_percent = 5

    def prompt_options(self) -> str:
        display_options: str = ", ".join(self.options)
        print(display_options)
        resp: str = prompt("Select Option (Default [c]): ")
        return resp.strip()

    def add_item(self):
        name = prompt("Item name: ")
        quantity = prompt("Quantity: ", int)
        price = prompt("Item price: ", float)
        print(f"{quantity} {name} with ${price} addded.")
        total_price = quantity * price
        item = {"name": name, "price": total_price, "quantity": quantity}
        self.items.append(item)
        self.total = self.total + total_price

    def calculate_tax(self) -> float:
        amount = self.total * (self.tax_percent / 100)
        return amount

    def format(self):
        # bill header
        display_bill = (
            f"\n\nBill: {self.customer_id}\n{'Item':20}{'Quantity':<12}{'Price':>12}"
        )
        display_bill = display_bill + "\n" + ("-" * 44)  # divider

        # add items
        for item in self.items:
            row = f"{item['name']:20}x {item['quantity']:<10}: {item['price']:>10.2f}"
            display_bill = display_bill + "\n" + row

        tax = self.calculate_tax()
        total_amount = self.total + tax

        display_bill = display_bill + "\n" + ("-" * 44)  # divider

        # bill footer
        display_bill = display_bill + "\n" + f"{'Tax':32}: {tax:10.2f}"
        display_bill = display_bill + "\n" + f"{'Total':32}: {total_amount:10.2f}"

        print(display_bill)


def main():
    print("Welcome to Simple POS")
    customer_id = prompt("Enter customer id: ")
    bill = Bill(customer_id)
    while True:
        selected = bill.prompt_options()
        if selected == "c":
            bill.add_item()
        elif selected == "s":
            bill.format()
            break
        else:
            bill.add_item()


if __name__ == "__main__":
    main()
